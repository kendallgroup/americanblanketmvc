﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Data;
using System.Data.Odbc;
using System.Xml;
using log4net;
using VolusionAPI.Utility;

namespace VolusionAPI
{
    class InsertCustomers
    {

        public string _rawData;
        public int UpdateCount;
        private static readonly ILog _Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public InsertCustomers(string rawData)
        {
            _Log.InfoFormat("Entered InserCustomers");
            _rawData = rawData;
            UpdateCustomersData();
        }

        public void UpdateCustomersData()
        {
            _Log.InfoFormat("Entered UpdateCustomersData Method");
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(_rawData);

            XmlNodeList nodeList = xmldoc.SelectNodes("/xmldata/Customers");
            _Log.Debug("starting loop of customers");
            foreach (XmlNode node in nodeList)
            {
                var customerid = node.SelectSingleNode("CustomerID").InnerText;
                var emailaddress = node.SelectSingleNode("EmailAddress").InnerText;
                var lastmod = node.SelectSingleNode("LastModified").InnerText;

                var DataInTable = checktable(customerid.Trim());

                if (DataInTable == 0)
                {
                    List<OdbcParameter> param = new List<OdbcParameter>();
                    param.Add(new OdbcParameter("CustomerID", customerid));
                    param.Add(new OdbcParameter("EmailAddress", emailaddress));
                    param.Add(new OdbcParameter("LastModified",lastmod));
                    string sql = "INSERT INTO api_customers (CustomerID, EmailAddress, LastModified) VALUES (?, ?, ?)";
                    Data.DataGun.ManipulateData(sql, param);
                    UpdateCount++;
                }

            }

            Utility.Utility.Logg("Customer Update Complete: " + UpdateCount + " Records Added", Utility.Utility.currentDateTime(), "Passed");
            _Log.Debug("Customer Update Complete: " + UpdateCount + " Records Added Passed");
        }


        public int checktable(string customerid)
        {
            string varsql = "SELECT * FROM api_customers WHERE customerID = ?";
            var cusparam = new OdbcParameter("@customeriID", customerid);
            DataTable result = Data.DataGun.GetDataMod(varsql, cusparam);

            if (result == null)
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }

        
    }

}
