﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Odbc;
using System.Net;
using System.Net.Http;
using System.Configuration;
using log4net;
using VolusionAPI.Data; 

namespace VolusionAPI
{
    class Program
    {
        private static readonly ILog _Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        static string staticUrl = "http://www.Americanblanketcompany.com/net/WebService.aspx?";
        static string staticUrl2 = "Login=Info@americanblanketcompany.com&EncryptedPassword=";
        static string ordersUrl2 = "&EDI_Name=Generic\\Orders&SELECT_Columns=*&WHERE_Column=o.OrderStatus&WHERE_Value=processing";
        static string customers = "&EDI_Name=Generic\\Customers&SELECT_Columns=*&WHERE_Column=AccessKey&WHERE_Value=C";
        static string ordersURL;
        static string customersURL;
        static string ordersData;
        static string customerData;
        static string vkey;

        static void Main(string[] args)
        {

            //logging
            _Log.InfoFormat("Start applicataion-Program.cs - 28 Api url=" + ordersURL);
            _Log.Debug("Number of parameters Entered " + args.Length);

            ////Call to get a database connection object
            //OdbcConnection varConn = Data.DataGun.getConnection();
            //var sql = "SELECT limit 1 FROM API_VKEY";

            //var varData = Data.DataGun.ManipulateData(sql);
            var volusionkey = Data.DataGun.GetVolusionKey();
           ordersURL = string.Format("{0}{1}{2}{3}", staticUrl, staticUrl2, volusionkey, ordersUrl2);
           customersURL = string.Format("{0}{1}{2}{3}", staticUrl, staticUrl2, volusionkey, customers);

            if (args.Length > 1)
            {
                Console.Write("Application can only take one Parameter Please try again\n");
                _Log.Error("Application can only take one string parameter");
                Console.Read();
            }

            if (args.Length == 0)
            {
                _Log.Debug("GetRequest for Orders Data");
                ordersData = Data.DataGun.GetRequest(ordersURL);
                var insertResponse = new InsertOrders(ordersData);

                //logging
                _Log.InfoFormat("Orders Download complete starting Customers " + insertResponse);

                customerData = Data.DataGun.GetRequest(customersURL);
                var customersResponse = new InsertCustomers(customerData);

                //logging
                _Log.InfoFormat("Application Function Complete.....application shutting down");
            }

            if (args.Length == 1)
            {
                switch (args[0].ToLower())
                {
                    case "orders":
                        _Log.Debug("GetRequest for Orders Data");
                        ordersData = Data.DataGun.GetRequest(ordersURL);
                        var insertResponse = new InsertOrders(ordersData);
                        _Log.InfoFormat("Orders Download complete starting Customers " + insertResponse);
                        break;
                    case "customers":
                        _Log.Debug("GetRequest for Customers Data");
                        customerData = Data.DataGun.GetRequest(customersURL);
                        var customersResponse = new InsertCustomers(customerData);
                        _Log.InfoFormat("Orders Download complete starting Customers " + customersResponse);
                        break;
                    default:
                        Console.Write("VolusionAPI Does Not Understand your Parameter string.  Please check your string for accuracy......Thanks! " + args[0]);
                        _Log.Debug("VolusionAPI Does Not Understand your Parameter string.  Please check your string for accuracy......Thanks!\n " + args[0]);
                    break;
                }
            }
        }
    }
}

