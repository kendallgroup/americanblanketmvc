﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Data.SqlClient;
using System.Data.Odbc;
using System.Data;
using log4net;
using System.Net.Http;
using Dapper;

namespace VolusionAPI.Data
{
    class DataGun
    {
        public static OdbcConnection _varConn { get; set; }
        private static readonly ILog _Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static OdbcConnection getConnection()
        {
            OdbcConnection conn = new OdbcConnection("DSN=DatawarehouseMySQL2");
            try
            {
                _varConn = conn;
            }
            catch (OdbcException ex)
            {
                _Log.Error("Error Opening ODBC connection", ex);
                throw new Exception(string.Format("There was a error when connection to the database: {0}", ex.Message));
            }
            _Log.InfoFormat("Connection Opened");
            _Log.Debug("Leaving getConnection Method/DataGun connection returned= " + conn);
            return conn;
        }

        public static DataTable GetDataMod(string query, OdbcParameter param)
        {
            //Logging
            _Log.Debug("Entering GetData Method query string passed in " + query);
            //Create DataTable
            DataTable dt = null;
            try
            {
                using (OdbcConnection conn = getConnection())
                {
                    using (OdbcCommand cmd = new OdbcCommand(query, conn))
                    {
                        //Open Connection
                        conn.Open();

                        //add Parameters
                        cmd.Parameters.AddWithValue(param.ParameterName, param.Value);
                        //create a odbcDataReader
                        OdbcDataReader rd = cmd.ExecuteReader();
                   
                        //OdbcDataAdapter rd = new OdbcDataAdapter(cmd);
                        if (rd.HasRows == true)
                        {
                            dt = new DataTable();
                            dt.Load(rd);
                        }
                    }
                }
            }
            catch (OdbcException ex)
            {
                _Log.Error("SQL Exception errors in GETDATA ", ex);
                throw new Exception("SQL errors in GETDATA", ex);
            }
            _Log.Debug("Leaving GetData data returned from Method " + dt);
            return dt;
        }

        public static DataTable GetData(string storedProc, List<OdbcParameter> param)
        {
            //Logging
            _Log.InfoFormat("Entering GetData For Stored Procudures");
            DataTable dt = null;
            try
            {
                using (OdbcConnection conn = getConnection())
                {
                    using (OdbcCommand cmd = new OdbcCommand(storedProc, _varConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        //Open Connection
                        _varConn.Open();

                        //Add parameters
                        if (param != null && param.Count > 10)
                        {
                            cmd.Parameters.AddRange(param.ToArray());
                        }

                        OdbcDataAdapter rd = new OdbcDataAdapter(cmd);
                        rd.Fill(dt);
                    }
                }
                _Log.Debug("Leaving GetData Method");
            }
            catch (OdbcException ex)
            {
                _Log.Error("Error in sql when running Stored procedure");
                throw new Exception("Error in sql when running Stored procedure", ex);
            }
            _Log.Debug("GetData returned: " + dt);
            return dt;
        }

        public static int ManipulateData(string query)
        {
            _Log.InfoFormat("Entering ManipulateData Method");
            //Define Variables
            int rowsAffected = 0;

            try
            {
                //Create SQL Connection
                using (OdbcConnection conn = getConnection())
                {
                    //Create a SQl Command Object
                    using (OdbcCommand cmd = new OdbcCommand(query, conn))
                    {
                        //open  connection
                        conn.Open();
                        
                        //execute sql
                        rowsAffected = cmd.ExecuteNonQuery();
                    }
                }
                
            }
            catch (OdbcException ex)
            {
                //logging error
                _Log.Error("SQL error when executing sql query", ex);
                //handle exception
                throw new Exception("SQL error when executing sql query", ex) ;
            }
            _Log.InfoFormat("leaving ManipulateData Method returned data " + rowsAffected);
            return rowsAffected;
        }

        public static int ManipulateData(string sql, List<OdbcParameter> param)
        {
            _Log.InfoFormat("Entering ManipulateData Method");
            //define Variables
            int rowsAffected = 0;

            try
            {
                using (OdbcConnection conn = getConnection())
                {
                    using (OdbcCommand cmd = new OdbcCommand(sql, conn))
                    {
                        //open connection
                        conn.Open();

                        //add parameters
                        {
                            cmd.Parameters.AddRange(param.ToArray());
                        }

                        // execute sql
                        rowsAffected = cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (OdbcException ex)
            {
                //logging error
                _Log.Error("Sql Error when executing sql query", ex);
                //handle exception
                throw new Exception("Sql Error when executing sql query", ex) ;
            }
            _Log.InfoFormat("leaving ManipulateData Method returned data " + rowsAffected);
            return rowsAffected;
        }

        public static string GetRequest(string url)
        {
            string result;
            _Log.InfoFormat("Entered GetRequest Method This url was passed " + url);
            using (HttpClient client = new HttpClient())
            {
                var response = client.GetAsync(url).Result;
                result = response.Content.ReadAsStringAsync().Result;
                _Log.Debug("response from request" + result);
            }
            _Log.Debug("Leaving the GetRequest Method - Program.cs - 46");
            return result;
        }

        public static string GetVolusionKey()
        {
            var varSQL = "Select * From datawarehouse.api_vkey where ID = 1";
            string vkey;

            using(IDbConnection connection = new MySql.Data.MySqlClient.MySqlConnection("Persist Security Info=False; database=datawarehouse; server=10.1.10.74; port = 3306; user id = DavidPerkins; pwd = Dwperk01"))
            {
                var volusionKey = connection.QueryFirst(varSQL);
                var newkey = volusionKey.vkey;
                vkey = newkey;
            }
            return vkey;
        }
    }
}
   