﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using log4net;

namespace VolusionAPI.Utility
{
    public static class Utility
    {

        private static readonly ILog _Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static int Logg(string message, string vardate, string status)
        {
            string varsql = "INSERT INTO api_log (message, vardate, status) VALUES ('" + message + "', '" + currentDateTime() + "', '" + status + "')";
            _Log.Debug("logg Method Insert query= " + varsql);
           
            try
            {
                Data.DataGun.ManipulateData(varsql);
            }
            catch (Exception ex)
            {
                //Log Error
                _Log.Error("Exceptiont during logg insert ", ex);
                //handle Exceptions

                return 0;
                throw new Exception("Exeptions during logg insert ", ex);
            }
            _Log.Debug("Leaving Logg Method returning " + 1);
            return 1;
        }

        public static string currentDateTime()
        {
            DateTime currentTime = DateTime.Now;
            string mySQLstr = currentTime.ToString("yyyy-MM-dd HH:mm:ss");
            return mySQLstr;
        }

    }
}
