﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace VolusionAPI
{
    [Serializable, XmlRoot("OrderDetails")]
    public class OrderDetails
    {
        public int OrderDetialID;
        public string Additional_Handling_Indicator;
        public string Affiliate_Commissionable_Value;
        public string AutoDropShip;
        public string CategoryID;
        public string CouponCode;
        public string CustomLineItem;
        public string DiscountAutoID;
        public string DiscountType;
        public string DiscountValue;
        public string DownloadFile;
        public string Fixed_ShippingCost;
        public string Fixed_ShippingCost_Outside_LocalRegion;
        public string Free_ShippingItem;
        public string GiftTrakNumber;
        public string GiftWrap;
        public string GiftWrapCost;
        public string GiftWrapNote;
        public string Height;
        public string IsKitID;
        public string KitID;
        public string LastModBy;
        public string LastModified;
        public string Length;
        public string Locked;
        public string OnOrder_Qty;
        public string OptionID;
        public string OptionIDs;
        public string Options;
        public string OrderDetialID_Third_Party;
        public string OrderDetialID_Third_Party_Link;
        public string OrderID;
        public string Oversized;
        public string Package_Type;
        public string Product_Keys_Shipped;
        public string ProductCdoe;
        public string ProductID;
        public string ProductName;
        public string ProductNote;
        public string ProductPrice;
        public string ProductWeight;
        public string QtyOnBackOrder;
        public string QtyOnHold;
        public string QtyOnPackingSlip;
        public string QtyShipped;
        public string Quanitity;
        public string Returned;
        public string Returned_Date;
        public string Reward_Points_Given_For_Purchase;
        public string RMA_Number;
        public string RMAI_ID;
        public string ShipDate;
        public string Shipped;
        public string Ships_By_Itself;
        public string TaxableProduct;
        public string TotalPrice;
        public string VAT_Percentage;
        public string Vendor_Price;
        public string Warehouses;
        public string Width;
    }
}
