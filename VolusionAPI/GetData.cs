﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Configuration;

namespace VolusionAPI
{
    class GetData
    {
        //Class that will pull the xml data into the program
        public HttpWebRequest request;
        public HttpWebResponse response;
      

        public GetData()
        {

        }

        //Post Request Example!!!!!
        async static void PostRequest(string url)
        {
            IEnumerable<KeyValuePair<string, string>> queries = new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("DataToSend", "Antonio Kendall"),
                new KeyValuePair<string, string>("MoreData","Post to Server")
            };
            HttpContent q = new FormUrlEncodedContent(queries);
            using (HttpClient client = new HttpClient())
            {
                using (HttpResponseMessage response = await client.PostAsync(url, q))
                {
                    using (HttpContent content = response.Content)
                    {
                        string mycontent = await content.ReadAsStringAsync();
                        Console.WriteLine(mycontent);

                    }
                }
            }
        }

        async static void GetRequest(string url)
        {
            using (HttpClient client = new HttpClient())
            {
                using (HttpResponseMessage response = await client.GetAsync(url))
                {
                    using (HttpContent content = response.Content)
                    {
                        string mycontent = await content.ReadAsStringAsync();
                        //ordersData = mycontent;
                        Console.WriteLine(mycontent);
                    }
                }
            }
        }

    }
}
