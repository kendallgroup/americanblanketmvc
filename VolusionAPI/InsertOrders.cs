﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Xml;
using VolusionAPI.Order;
using System.Text;
using log4net;
using VolusionAPI.Data;
using VolusionAPI.Utility;


namespace VolusionAPI
{
    class InsertOrders
    {

        private string _rawData;
        private static readonly ILog _Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public InsertOrders(string rawData)
        {
            _Log.Debug("Entering the InsertOrders Constructor - Data passed in= " + rawData);
            //Store raw XML String
            _rawData = rawData;
            //Truncating api_order and api_orderDetails
            dropTables();
            GetListofOrdersAndOrderDetails();
          
        }

        public void dropTables()
        {
            _Log.InfoFormat("Entered dropTables Method/InsertOrders");
            //sql statements/commands
            Data.DataGun.ManipulateData("TRUNCATE TABLE api_Orders");
            Data.DataGun.ManipulateData("TRUNCATE TABLE api_OrderDetails");
            _Log.InfoFormat("Leaving dropTables Method/InsertOrders");
        }

        public void GetListofOrdersAndOrderDetails()
        {
            _Log.InfoFormat("Entering GetListofOrdersAndOrderDetails");
            try
            {
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(_rawData);
                XmlNodeList orders = xml.SelectNodes("/xmldata/Orders");

                // Dictionary<string, string> ordersDic = new Dictionary<string, string>();
                foreach (XmlNode order in orders)
                {
                    var keylist = new List<string>();
                    var valuelist = new List<string>();
                    List<OdbcParameter> param = new List<OdbcParameter>();
                    foreach (XmlElement prop in order)
                    {
                        
                        if (prop.Name != "OrderDetails")
                        {
                            keylist.Add(prop.Name);
                            param.Add(new OdbcParameter(prop.Name, prop.InnerText));
                            valuelist.Add("?");
                        }

                        if (prop.Name == "TrackingNumbers")
                        {
                            XmlNodeList tracking = prop.SelectNodes("/Order/TrackingNumbers");
                            foreach (XmlNode track in tracking)
                            {
                                switch (track.Name)
                                {
                                    case "Gateway":
                                        keylist.Add(track.Name);
                                        param.Add(new OdbcParameter(track.Name, track.InnerText));
                                        valuelist.Add("?");
                                        break;
                                    case "TrackingNumber":
                                        keylist.Add(track.Name);
                                        param.Add(new OdbcParameter(track.Name, track.InnerText));
                                        valuelist.Add("?");
                                        break;
                                    case "Shipment_Cost":
                                        keylist.Add(track.Name);
                                        param.Add(new OdbcParameter(track.Name, track.InnerText));
                                        valuelist.Add("?");
                                        break;
                                }

                            }
                        }

                    }
                    string keys = string.Join(",", keylist);
                    string value = string.Join(",", valuelist);
                    var result = insertData("api_orders", keys, value, param);
                    keylist.Clear();
                    valuelist.Clear();
                }

                XmlNodeList orderDetails = xml.SelectNodes("/xmldata/Orders/OrderDetails");
                Utility.Utility.Logg("Completed Writing Orders", Utility.Utility.currentDateTime(), "Passed");
                foreach (XmlNode orderDetail in orderDetails)
                {
                    var keylist = new List<string>();
                    var valuelist = new List<string>();
                    List<OdbcParameter> param = new List<OdbcParameter>();

                    foreach (XmlElement prop in orderDetail)
                    {
                        if (prop.Name != "OrderDetails_Options")
                        {
                            keylist.Add(prop.Name);
                            param.Add(new OdbcParameter(prop.Name, prop.InnerText));
                            valuelist.Add("?");
                        }
                    }

                    string keys = string.Join(",", keylist);
                    string value = string.Join(",", valuelist);
                    var result = insertData("api_orderdetails", keys, value, param);
                    keylist.Clear();
                    valuelist.Clear();
                }
            }
            catch (XmlException ex)
            {
                _Log.Error("There was a problem during Deserialization " + ex);
                Utility.Utility.Logg("issues writing orders/orderdetails to database", Utility.Utility.currentDateTime(), "Failed");
                throw new Exception("There was a problem during Deserialization", ex);
            }
            _Log.InfoFormat("leaving GetListofOrdersAndOrderDetails Method/InsertOrders");
            Utility.Utility.Logg("Completed Writing Order Details", Utility.Utility.currentDateTime(), "Passed");
        }


        public int insertData(string tableName, string columlist, string columnValues, List<OdbcParameter> param)
        {
            _Log.InfoFormat("Entering the insertData Method/InsertOrders");

            int result;
            string sql = string.Format("INSERT INTO {0} ({1}) VALUES ({2})", tableName, columlist, columnValues);
            _Log.Debug(sql);
            try
            {
                result = Data.DataGun.ManipulateData(sql, param);
            }
            catch (OdbcException ex)
            {
                _Log.Error("A problem when inserting records", ex);
                Utility.Utility.Logg("Completed writing orders to database", Utility.Utility.currentDateTime(), "Failed");
                throw new Exception("A problem when inserting records", ex);
            }
            _Log.Debug("Returned from insertData " + result);
            _Log.Debug("leaving insertData");
            return result;
        }
    }
}
